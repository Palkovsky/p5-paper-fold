var currentScreen;

function setup() {
	createCanvas(window.innerWidth, window.innerHeight);
	currentScreen = new PlayScreen(random(LEVELS));
}

function draw() {
	currentScreen.draw();
}


function mousePressed(e){
	currentScreen.mousePressed(e);
}

function touchMoved(e){
	currentScreen.touchMoved(e);
}

function mouseWheel(e){
	currentScreen.mouseWheel(e);
}

function mouseReleased(e){
	currentScreen.mouseReleased(e);
}

function keyPressed(e){
	currentScreen.keyPressed(e);
}