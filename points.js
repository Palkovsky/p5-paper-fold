function findAngle(startX, startY, endX, endY){
	var a =  Math.atan2(endY - startY, endX - startX) * 180 / Math.PI;
	return a;
}

function distance(startX, startY, endX, endY){
	return Math.sqrt((endX-=startX)*endX + (endY-=startY)*endY);
}