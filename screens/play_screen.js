function PlayScreen(level){
	this.currentLevel = level;

	this.WIDTH = 210 * 4;
 	this.HEIGHT = 148 * 4;

	this.BORDER_WIDTH = window.innerWidth / 1.7;
	this.BORDER_HEIGHT = window.innerHeight - 50;

	this.newLayers = [];
	this.layers = [];
	this.layersHistory = [];
	this.displayLayers = [];
	this.folds = 0;
	this.maxDistance = 0;
	this.completeness = 0;
	this.initial90 = false;
	this.gameFinished = false;
	this.gameSuccess = false;
	this.border = {
		x: window.innerWidth / 2 - this.BORDER_WIDTH/2, y: window.innerHeight/2 - this.BORDER_HEIGHT/2,
		width: this.BORDER_WIDTH, height: this.BORDER_HEIGHT, area: this.BORDER_WIDTH * this.BORDER_HEIGHT
	};
	
	this.layers.push({
		v: this.currentLevel["paper-vertices"],
		flip: true
	});
	this.targetPolygonVertices = this.currentLevel["target-vertices"];
	this.layersHistory.push(this.layers);

	this.draw = function() {
		textSize(32);
		background(100);
		stroke(255);

		this.newLayers = [];
		this.displayLayers = [];
		for(var i = 0; i < this.layers.length; i++){
			this.displayLayers.push({v: this.layers[i].v.slice(), flip: this.layers[i].flip})
		}

		if(this.anchorPoint && this.touchPoint){
			fill(255);
			ellipse(this.anchorPoint.x, this.anchorPoint.y, 15, 15);
			fill(0, 170, 69);
			ellipse(this.touchPoint.x, this.touchPoint.y, 15, 15);

			var angle = findAngle(this.anchorPoint.x, this.anchorPoint.y, this.touchPoint.x, this.touchPoint.y);
			var midPoint = {x: (this.anchorPoint.x + this.touchPoint.x) / 2, y: (this.anchorPoint.y + this.touchPoint.y) / 2};

			if(this.initial90 && angle != 0){
				this.initial90 = false;
			}

			if(!this.initial90){

				angle = (angle % 90 == 0) ? angle + 0.1 : angle;
				angle = (angle % 45 == 0) ? angle + 0.1 : angle;
				angle = (angle % 30 == 0) ? angle + 0.1 : angle;

		 		var startLineVertices = vLineFromAngle(this.anchorPoint.x, this.anchorPoint.y, angle + 90, 1500);
		 		var midLineVertices = vLineFromAngle(midPoint.x, midPoint.y, angle + 90, 1500);
		 		var endLineVertices = vLineFromAngle(this.touchPoint.x, this.touchPoint.y, angle + 90, 1500);
		 		
		 		var sliceA = {x: midLineVertices[0], y: midLineVertices[1]};
		 		var sliceB = {x: midLineVertices[2], y: midLineVertices[3]};

		 		fill(255, 0, 0);
		 		ellipse(sliceA.x, sliceA.y, 15, 15);
		 		fill(0, 0, 255);
		 		ellipse(sliceB.x, sliceB.y, 15, 15);

		 		drawVertices(midLineVertices);
		 		drawVertices(endLineVertices);

		 		//fill(255, 0, 0);
		 		//drawVertices([sliceA.x, sliceA.y, sliceB.x, sliceB.y, 0, window.innerHeight]);

		 		var mirroredCount = 0;

		 		fill(50, 255, 50);
		 		for(var i = this.displayLayers.length - 1; i >= 0; i--){
		 			var layer = this.displayLayers[i].v;
		 			var slices = PolyK.Slice(layer, sliceA.x, sliceA.y, sliceB.x, sliceB.y);
		 			
		 			if(slices.length == 1){
		 				var newLayer = slices[0];
		 				if(areVerticesUnder(midPoint.x, midPoint.y, angle + 90, newLayer, 1500)){
		 					var flipped = flipVertices(midPoint.x, midPoint.y, angle + 90, newLayer, 1500);
		 					this.newLayers.push({v: flipped, flip: !this.displayLayers[i].flip});
		 					this.displayLayers.splice(i, 1);
		 					mirroredCount += 1;
		 				}
		 			}else if(slices.length == 2){
		 				
		 				var newLayer;
		 				var updatedExistingLayer;
		 				var lineAngle = angle + 90;

		 				if(!this.displayLayers[i].flip){
		 					newLayer = slices[0];
		 					updatedExistingLayer = slices[1];
		 				}else{
		  					newLayer = slices[1];
		 					updatedExistingLayer = slices[0];					
		 				}

		 				var flipped = flipVertices(midPoint.x, midPoint.y, angle + 90, newLayer, 1500);
		 				this.newLayers.push({v: flipped, flip: !this.displayLayers[i].flip});
		 				this.displayLayers[i].v = updatedExistingLayer;
		 			}
		 		}

		 		if(mirroredCount == this.layers.length || !this.areLayersWithinBorder()){
		 			this.interruptFold();
		 		}
		 		this.calculateCompleteness();
			}

			noStroke();
			fill(0, 255, 0);
			text("A: " + Math.round(angle + 90), 20, 220);
		}else{
			//check for endgame
			if(this.completeness > 95){
				this.gameSuccess = this.gameFinished = true;
			}else if(this.folds == this.currentLevel['fold-limit']){
				this.gameFinished = true;
				this.gameSuccess = false;
			}
		}

		stroke(0, 0, 0);
		fill(69, 0, 170, 50, 12);
		for(var i = 0; i < this.displayLayers.length; i++){
			drawVertices(this.displayLayers[i].v);
		}

		for(var i = 0; i < this.newLayers.length; i++){
			drawVertices(this.newLayers[i].v);
		}

		stroke(255);
		noFill();
		drawVertices(this.targetPolygonVertices);

		//Draw control border
		strokeWeight(4);
		stroke(0);
		rect(this.border.x, this.border.y, this.border.width, this.border.height);
		strokeWeight(1);

		noStroke();
		fill(50, 255, 50);
		text(this.currentLevel.name, 20, 60);
		text("FOLDS: " + this.folds, 20, 100);
		text("MAX FOLDS: " + this.currentLevel['fold-limit'], 20, 140);
		text("COMPLETENESS: " + this.completeness + "%", 20, 180);

		if(this.gameFinished){
			textSize(40);
			if(this.gameSuccess){
				fill(0, 0, 255);
				text("YOU WON. CONGRATULATIONS.", window.innerWidth/2 - 360, window.innerHeight/2 - 50);
			}else{
				fill(255, 0, 0);
				text("GAME OVER. BACKSPACE TO FOLD BACK.", window.innerWidth/2 - 360, window.innerHeight/2 - 50);
			}
		}
	}

	this.interruptFold = function(){
		this.anchorPoint = null;
		this.touchPoint = null;
		this.initialPoint = null;
		this.mouseReleased();
	}

	this.mousePressed = function(e){
		if(!this.gameFinished){
			this.touchPoint = {x: mouseX, y: mouseY};
			this.initialPoint = {x: mouseX, y: mouseY};
			this.initial90 = true;
		}
	}

	this.touchMoved = function(e){
		if(this.initialPoint != null){
			this.touchPoint = {x: mouseX, y: mouseY};
			if(this.anchorPoint == null){
				this.anchorPoint = {x: mouseX, y: mouseY};
			}
		}
	}

	this.mouseReleased = function(e){
		if(this.anchorPoint != null && this.touchPoint != null){
			this.layers = this.displayLayers;
			for(var i = 0; i < this.newLayers.length; i++){

				if(this.newLayers[i].v.length > 4 && abs(PolyK.GetArea(this.newLayers[i].v)) > 5){
					this.layers.push(this.newLayers[i]);
				}
			}
			this.layersHistory.push(this.layers);
			this.folds = (this.newLayers.length > 0) ? this.folds + 1 : this.folds;
			this.anchorPoint = null;
			this.touchPoint = null;
			this.initialPoint = null;
			this.initial90 = false;
			this.newLayers = [];
			this.displayLayers = [];
		}
	}

	this.keyPressed = function(e){
		switch(e.key){
			case "Backspace":
				if(this.layersHistory.length > 1 && !this.anchorPoint && !this.touchPoint){
					this.layers = this.layersHistory[this.layersHistory.length - 2];
					this.layersHistory.pop();
					this.folds--;
					this.gameFinished = false;
					this.gameSuccess = false;
					this.calculateCompleteness();
				}
				break;
		}
	}

	this.calculateCompleteness = function(){
		noFill();
	 	stroke(255, 0, 0);

	 	var totalDistance = 0;
	 	var testLayers = [];

	 	if(this.anchorPoint && this.touchPoint){
	 		testLayers = this.displayLayers.slice();
	 		var newNewLayers = this.newLayers.slice();
	 		for(var i = 0; i < newNewLayers.length; i++){
	 			testLayers.push(newNewLayers[i]);
	 		}
	 	}else{
	 		testLayers = this.layers.slice();
	 	}

	 	var largestAABB = null;
	 	for(var i = 0; i < testLayers.length; i++){
	 		
	 		var layer = testLayers[i].v;
	 		var aabb = findAABB(layer);
	 		largestAABB = (largestAABB === null || aabb.area > largestAABB.area) ? aabb : largestAABB;

	 		for(var j = 0; j < layer.length; j += 2){

	 			var vertex = {x: layer[j], y: layer[j + 1]};
				var closest = PolyK.ClosestEdge(this.targetPolygonVertices, vertex.x, vertex.y);
				var distance = dist(closest.point.x, closest.point.y, vertex.x, vertex.y);

			 	if(!PolyK.ContainsPoint(this.targetPolygonVertices, vertex.x, vertex.y)){
			 		totalDistance += distance * 2;
				}
	 		}
	 	}

	 	var aabbDiff = 0;
	 	if(largestAABB != null){
	 		noFill();
	 		var targetAABB = findAABB(this.targetPolygonVertices);
	 		stroke(0, 255, 0);
	 		rect(targetAABB.x, targetAABB.y, targetAABB.width, targetAABB.height);
	 		stroke(255, 0, 0);
	 		rect(largestAABB.x, largestAABB.y, largestAABB.width, largestAABB.height);
	 		aabbDiff = abs(targetAABB.area - largestAABB.area);
	 		aabbDiff += dist(targetAABB.x, targetAABB.y, largestAABB.x, largestAABB.y) * 1.5;
		}

		totalDistance += map(Math.sqrt(abs(aabbDiff)), 0, Math.sqrt(abs(this.border.area)), 0, 000);
	 	this.maxDistance = (totalDistance > this.maxDistance) ? totalDistance : this.maxDistance;
		this.completeness = Math.ceil(map(totalDistance, 5, this.maxDistance, 100, 0));
		this.completeness = (this.completeness < 0 || totalDistance > this.maxDistance/2) ? 0 : this.completeness;
		this.completeness = (this.completeness > 100) ? 100 : this.completeness;
	}

	this.areLayersWithinBorder = function(){
	 	var testLayers = this.displayLayers.slice();
	 	var newNewLayers = this.newLayers.slice();
	 	for(var i = 0; i < newNewLayers.length; i++){
	 		testLayers.push(newNewLayers[i]);
	 	}

	 	for(var i = 0; i < testLayers.length; i++){
	 		var layer = testLayers[i].v;
	 		for(var j = 0; j < layer.length; j += 2){
	 			var vertex = {x: layer[j], y: layer[j+1]};

	 			if(!(vertex.x > this.border.x && vertex.x < this.border.x + this.border.width && vertex.y > this.border.y && vertex.y < this.border.y + this.border.height)){
	 				return false;
	 			}
	 		}
	 	}

	 	return true;
	}
}