/**
	Vertices calculation functions.
**/

function vLine(startX, startY, endX, endY){
	return [startX, startY, endX, endY];
}

//This function calculates vertices for line passing through x, y point with given angle.
function vLineFromAngle(x, y, deg, length){
	length = (length === undefined) ? 1000 : length;
	var endX = x + length * Math.cos(deg * Math.PI / 180);
	var startX = x - length * Math.cos(deg * Math.PI / 180);
	var endY = y + length * Math.sin(deg * Math.PI / 180);
	var startY = y - length * Math.sin(deg * Math.PI / 180);
	return vLine(startX, startY, endX, endY);
}

function vRectangle(x, y, width, height){
	return [x, y - height, x, y, x + width, y, x + width, y - height];
}

function vEquilateralTraingle(x, y, a){
	var h = (a * Math.sqrt(3)) / 2;

	var pointA = {x: (x - a / 2), y: (y + h / 3)};
	var pointB = {x: (x + a / 2), y: (y + h / 3)};
	var pointC = {x: x, y: (y - 2 * h / 3)};

	return [pointA.x, pointA.y, pointB.x, pointB.y, pointC.x, pointC.y];
}

function vRegularPolygon(x, y, r, n){
	var vertices = [];
	for (var i = 0; i < n; i++) {
	    vertices.push(x + r * Math.cos(2 * Math.PI * i / n));
	    vertices.push(y + r * Math.sin(2 * Math.PI * i / n));
	}
	return vertices;
}

/**
	Vertices utility functions.
**/
function drawVertices(vertices){
	beginShape();
 	for(var i = 0; i < vertices.length; i += 2){
 		vertex(vertices[i], vertices[i + 1]);
 	}
 	endShape(CLOSE);
}

function closestVertex(x, y, vertices, min_distance){
	var closest = null;
	min_distance = (min_distance === undefined) ? 1000000 : min_distance;

	for(var i = 0; i < vertices.length; i += 2){
		var point = {x: vertices[i], y: vertices[i + 1], distance: dist(x, y, vertices[i], vertices[i + 1])};

		if(((closest !== null && closest.distance > point.distance) || closest === null) && point.distance <= min_distance){
			closest = point;
		}
	}

	return closest;
}

//It finds all vertices ender the line passing x, y
function areVerticesUnder(x, y, deg, vertices, length){
	length = (length === undefined) ? 1000 : length;

	var endX = x + length * Math.cos(deg * Math.PI / 180);
	var startX = x - length * Math.cos(deg * Math.PI / 180);
	var endY = y + length * Math.sin(deg * Math.PI / 180);
	var startY = y - length * Math.sin(deg * Math.PI / 180);

	for(var i = 0; i < vertices.length; i += 2){
		var vertex = {x: vertices[i], y: vertices[i+1]};
		var left = isLeft({x: startX, y: startY}, {x: endX, y: endY}, {x: vertex.x, y: vertex.y});
		if(left){
			return true;
		}
	}

	return false;
}

function areVerticesOn(x, y, deg, vertices, length){
	length = (length === undefined) ? 1000 : length;

	var endX = x + length * Math.cos(deg * Math.PI / 180);
	var startX = x - length * Math.cos(deg * Math.PI / 180);
	var endY = y + length * Math.sin(deg * Math.PI / 180);
	var startY = y - length * Math.sin(deg * Math.PI / 180);

	for(var i = 0; i < vertices.length; i += 2){
		var vertex = {x: vertices[i], y: vertices[i+1]};
		var on = isOn({x: startX, y: startY}, {x: endX, y: endY}, {x: vertex.x, y: vertex.y});
		if(on){
			return true;
		}
	}

	return false;
}

function flipVertices(x, y, deg, vertices, length){
	length = (length === undefined) ? 1000 : length;
	var endX = x + length * Math.cos(deg * Math.PI / 180);
	var startX = x - length * Math.cos(deg * Math.PI / 180);
	var endY = y + length * Math.sin(deg * Math.PI / 180);
	var startY = y - length * Math.sin(deg * Math.PI / 180);
	var flipped = [];

	for(var i = 0; i < vertices.length; i += 2){
		var vertex = {x: vertices[i], y: vertices[i+1]};
		var reflected = reflect(vertex, {x: endX, y: endY}, {x: startX, y: startY});
		flipped.push(reflected.x);
		flipped.push(reflected.y);
	}

	return flipped;
}

//Determines if c is on left side of line crossing a and b.
function isLeft(a, b, c){
	return Math.sign((b.x - a.x) * (c.y - a.y) - (b.y - a.y) * (c.x - a.x)) > 0;
}

//Determines if c is on left side of line crossing a and b.
function isRight(a, b, c){
	return Math.sign((b.x - a.x) * (c.y - a.y) - (b.y - a.y) * (c.x - a.x)) < 0;
}

//Determines if c is on left side of line crossing a and b.
function isOn(a, b, c){
	return Math.sign((b.x - a.x) * (c.y - a.y) - (b.y - a.y) * (c.x - a.x)) == 0;
}

function reflect(p, p0, p1) {
        var dx, dy, a, b, x, y;

        dx = p1.x - p0.x;
        dy = p1.y - p0.y;
        a = (dx * dx - dy * dy) / (dx * dx + dy * dy);
        b = 2 * dx * dy / (dx * dx + dy * dy);
        x = Math.round(a * (p.x - p0.x) + b * (p.y - p0.y) + p0.x); 
        y = Math.round(b * (p.x - p0.x) - a * (p.y - p0.y) + p0.y);

        return {x: x, y: y};
 }

 function containsVertex(x, y, vertices){
	var found = false;
	for(var i = 0; i < vertices.length; i += 2){
		if(x == vertices[i] && y == vertices[i+1]){
			found = true;
			break;
		}
	}
	return found;
}

function findAABB(vertices){
	var lowestX = NaN;
	var highestX = NaN;
	var lowestY = NaN;
	var highestY = NaN;

	for(var i = 0; i < vertices.length; i += 2){
		lowestX = (isNaN(lowestX) || vertices[i] < lowestX) ? vertices[i] : lowestX;
		highestX = (isNaN(highestX) || vertices[i] > highestX) ? vertices[i] : highestX;
		lowestY = (isNaN(lowestY) || vertices[i + 1] < lowestY) ? vertices[i + 1] : lowestY;
		highestY = (isNaN(highestY) || vertices[i + 1] > highestY) ? vertices[i + 1] : highestY;
	}

	return {
		x: lowestX, 
		y: lowestY, 
		width: (highestX - lowestX), 
		height: (highestY - lowestY),
		area: (highestX - lowestX) * (highestY - lowestY)
	};
}