var LEVELS = [
	{
		"name" : "Small Triangle",
		"fold-limit" : 5,
		"paper-vertices" : vRegularPolygon(window.innerWidth / 2, window.innerHeight / 2, 300, 6),
		"target-vertices" : vRegularPolygon(window.innerWidth / 2, window.innerHeight / 2, 200, 3)
	},		{
		"name" : "Easy Triangle",
		"fold-limit" : 3,
		"paper-vertices" : vRegularPolygon(window.innerWidth / 2, window.innerHeight / 2, 300, 6),
		"target-vertices" : vRegularPolygon(window.innerWidth / 2, window.innerHeight / 2, 300, 3)
	},
	{
		"name" : "Trapeeze",
		"fold-limit" : 7,
		"paper-vertices" : vRegularPolygon(window.innerWidth / 2, window.innerHeight / 2, 300, 6),
		"target-vertices" : [
			window.innerWidth / 2 - 100, window.innerHeight/2,
			window.innerWidth / 2 + 100, window.innerHeight/2,
			window.innerWidth / 2 + 50, window.innerHeight/2 + 100,
			window.innerWidth / 2 - 50, window.innerHeight/2 + 100
		]
	},
	{
		"name" : "Half Square",
		"fold-limit" : 1,
		"paper-vertices" : [
			window.innerWidth / 2 - 300, window.innerHeight/2 + 300,
			window.innerWidth / 2 - 300, window.innerHeight/2 - 300,
			window.innerWidth / 2 + 300, window.innerHeight/2 - 300,
			window.innerWidth / 2 + 300, window.innerHeight/2 + 300
		],
		"target-vertices" : [
			window.innerWidth / 2 - 300, window.innerHeight/2 + 300,
			window.innerWidth / 2 - 300, window.innerHeight/2 - 300,
			window.innerWidth / 2 + 300, window.innerHeight/2 - 300
		]
	}
]